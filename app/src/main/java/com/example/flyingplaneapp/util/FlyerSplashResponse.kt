package com.example.flyingplaneapp.util

import androidx.annotation.Keep

@Keep
data class FlyerSplashResponse(val url : String)