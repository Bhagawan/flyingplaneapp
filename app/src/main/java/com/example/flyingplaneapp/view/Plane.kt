package com.example.flyingplaneapp.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.graphics.drawable.toBitmap
import com.example.flyingplaneapp.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.math.acos
import kotlin.math.pow
import kotlin.math.sign
import kotlin.math.sqrt

class Plane(context: Context, attributeSet: AttributeSet?) : View(context, attributeSet) {
    private var mHeight = 0
    private var mWidth = 0
    private var planeCoords = Point(0,height / 2)
    private val plane = BitmapFactory.decodeResource(context.resources, R.drawable.plane)
    private val back = BitmapFactory.decodeResource(context.resources, R.drawable.background)
    private val restart = AppCompatResources.getDrawable(context,R.drawable.ic_baseline_replay)?.toBitmap()
    private var planeAngle = -30.0f

    private var dX = 0.0f
    private var dY = 0.0f

    private var state = START

    private var startDragX = 0.0f
    private var startDragY = 0.0f

    companion object {
        const val START = 0
        const val FLYING = 1
        const val FINISH = 2
    }

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            planeCoords.y = mHeight / 3
        }
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            if(state == FLYING) updatePlane()
            drawPlane(it)
            drawGround(it)
            drawUI(it)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                when(state) {
                    START -> {
                        startDragX = event.x
                        startDragY = event.y
                    }
                    FLYING -> {

                    }
                    FINISH -> {

                    }
                }
                return true
            }
            MotionEvent.ACTION_UP -> {
                when(state) {
                    START -> {
                        dX = ((event.x - startDragX) / 30).coerceAtLeast(0.0f)
                        dY = ((event.y - startDragY) / 30)
                        state = FLYING
                    }
                    FLYING -> {

                    }
                    FINISH -> {
                        restart()
                    }
                }
                return true
            }
        }

        return super.onTouchEvent(event)
    }

    private fun drawPlane(c : Canvas) {
        val rotationMatrix = Matrix()
        rotationMatrix.postRotate(planeAngle)
        val rotatedPlane = Bitmap.createBitmap(plane, 0,0,plane.width, plane.height, rotationMatrix, true )
        val p = Paint()
        p.color = Color.WHITE
        c.drawBitmap(rotatedPlane, planeCoords.x.toFloat().coerceAtMost(mWidth * 0.6f), planeCoords.y.toFloat().coerceAtLeast(mHeight * 0.2f), p)
    }

    private fun drawGround(c : Canvas) {
        if(planeCoords.y >= -20) {
            val p = Paint()
            p.color = Color.WHITE
            p.strokeWidth = 3.0f
            val y =(mHeight - planeCoords.y - 50.0f).coerceAtLeast(mHeight - 20.0f)
            c.drawLine(0.0f, y, mWidth.toFloat(), y, p)
        }
    }

    private fun drawUI(c: Canvas) {
        val p = Paint()
        p.color = Color.WHITE
        p.strokeWidth = 3.0f
        p.textSize = 90.0f
        p.textAlign = Paint.Align.CENTER

        when(state) {
            START -> {
                c.drawText("Запустите самолет", mWidth / 2.0f, mHeight / 5.0f, p)
                c.drawLine(plane.width.toFloat() + 50, planeCoords.y + plane.height * 0.5f, mWidth / 2.0f, mHeight * 0.3f, p)
                c.drawLine(mWidth / 2.0f - 40, mHeight * 0.3f - 10, mWidth / 2.0f, mHeight * 0.3f, p)
                c.drawLine(mWidth / 2.0f - 10, mHeight * 0.3f + 20, mWidth / 2.0f, mHeight * 0.3f, p)
            }
            FLYING -> {
                c.drawText("Дистанция: ${planeCoords.x / 10} м", mWidth / 2.0f, mHeight * 0.07f, p)
            }
            FINISH -> {
                c.drawText("Самолет пролетел: ${planeCoords.x / 10} м", mWidth * 0.5f, mHeight * 0.3f, p)
                restart?.let { c.drawBitmap(restart, (mWidth - restart.width) * 0.5f  , (mHeight - restart.height) * 0.5f, p) }
            }
        }
    }

    private fun updatePlane() {
        dY += 0.1f
        val newX = planeCoords.x + dX
        val newY = planeCoords.y + dY

        val x = newX - planeCoords.x
        val y = newY - planeCoords.y
        planeAngle =  Math.toDegrees((sign(y) *acos(x / sqrt(x.pow(2) + y.pow(2)))).toDouble()).toFloat()
        planeCoords.set(newX.toInt(), newY.toInt())

        if(planeCoords.y >  mHeight - 50 -  plane.height * 0.6f) state = FINISH
    }

    private fun restart() {
        state = START
        planeCoords.set(0, mHeight / 2)
        planeAngle = -30.0f
    }


}